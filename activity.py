# Create 5 variables and output them in the command prompt

name = "Larfy"
age = 36
occupation = "Content Creator"
movie = "Black Panter"
rating = 95

print("I am ", name, ", and I am ", age, " years old, I work as a ", occupation, ", and my rating for ", movie, " is ", rating, "%")

# Create 3 variables

num1 = 3
num2 = 6
num3 = 9

# a. Get the product of num1 and num2

print("The product of ", num1, " and ", num2, " is ", num1 * num2)

# b. Check if num1 is less than num3

if(num1 < num3):
	print("num1 is less than num3")
else:
	print("num1 is greater than num3")

# c. Add the value of num3 to num2

print(num2)

print("Adding value of num3(", num3, ") to num2(", num2, ")")
num2 += num3

print(num2)